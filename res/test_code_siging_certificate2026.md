# Test Code Signing Certificate

To test the CI pipeline and particulary the code signing of the ESysRepo executables, DLLs and the Windows installer, it's usefull to have a self-signed code siging certificate.

## Creating the self-signed code siging certificate
It was created in PowerShell with the following command:
```
$cert = New-SelfSignedCertificate -DNSName "www.ultraduino.com" -CertStoreLocation Cert:\CurrentUser\My -Type CodeSigningCert -Subject “Test UltraDuino” -FriendlyName "Test UltraDuino Code Signing Certificate" -NotAfter (Get-Date).AddMonths(36)
```
After it's creation, you can display some information about it and most importantly it's thumbprint, by typing
```
$cert
```
This will print something like this:
```
   PSParentPath: Microsoft.PowerShell.Security\Certificate::CurrentUser\My

Thumbprint                                Subject
----------                                -------
929B2299EBCAC2449594DDF20AF21C613C0A3B58  CN=Test UltraDuino
```
## Checking the created certificate
Note that to be able to see your certificates, you should search for the run application, and in it start the application "certmgr.msc".
![run certmgr.msc](run_certmgr.jpg "Run certmgr.msc")
