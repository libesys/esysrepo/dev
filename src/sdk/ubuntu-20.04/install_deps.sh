#!/bin/bash

echo "Install dependencies for ESysRepo SDK ..."

apt -y install bzip2
apt -y install libpocofoundation62
apt -y install libpocoxml62
apt -y install libssh2-1
apt -y install libboost-filesystem1.71.0
apt -y install libboost-program-options1.71.0
apt -y install libssl1.1
apt -y install libgit2
apt -y install libxml2
apt -y install libxmlsec1
apt -y install libxmlsec1-openssl

echo "Install dependencies done."
