#!/bin/bash

echo "TXT_SInstall dependencies for ESysBuild SDK ..."

apt -y install bzip2
apt -y install libpocofoundation80
apt -y install libpocoxml80
apt -y install libssh2-1
apt -y install libboost-filesystem1.74.0
apt -y install libboost-program-options1.74.0
apt -y install libssl1.1
apt -y install libgit2
apt -y install libxml2
apt -y install libxmlsec1
apt -y install libxmlsec1-openssl

echo "Install dependencies done."
