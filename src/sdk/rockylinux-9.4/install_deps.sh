#!/bin/bash

echo "Install dependencies for ESysRepo SDK ..."

dnf -y install epel-release
dnf config-manager --enable crb

dnf -y install bzip2
dnf -y install poco-foundation
dnf -y install poco-xml
dnf -y install boost-iostreams
dnf -y install boost-filesystem
dnf -y install boost-program-options
dnf -y install boost-regex
dnf -y install boost-thread
dnf -y install boost-date-time
dnf -y install boost-chrono
dnf -y install boost-atomic

dnf -y install xmlsec1
dnf -y install xmlsec1-openssl

echo "Install dependencies done."
