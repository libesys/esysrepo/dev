#!/bin/bash

echo "Install dependencies for ESysRepo SDK ..."

# Need to have add-apt-repository
apt -y install software-properties-common

# Needed because of the usage of GCC 9
add-apt-repository -y ppa:ubuntu-toolchain-r/test

# Needed for newer version of Boost
add-apt-repository -y ppa:mhier/libboost-latest

apt -y update
apt -y upgrade

# Install GCC 9
apt -y install gcc-9 g++-9
apt -y install libstdc++6

apt -y install libpocofoundation50
apt -y install libpocoxml50

apt -y install libboost1.70
apt -y install libssh2-1
apt -y install libssl1.1
apt -y install libgit2
apt -y install libxml2
apt -y install libxmlsec1
apt -y install libxmlsec1-openssl

echo "Install dependencies done."
