#
# cmake-format: off
# __legal_b__
#
# Copyright (c) 2020-2021 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
#
# __legal_e__
# cmake-format: on
#

add_subdirectory(esysbase)
add_subdirectory(esysfile)
add_subdirectory(esyslog)
add_subdirectory(esystrace)

if(SUPERBUILD_BIN_SDK)
  set(ESYSREPO_BUILD_UNIT_TESTS On)
  set(ESYSREPO_BUILD_DOC On)

  install(FILES sdk/init_sdk DESTINATION ${CMAKE_INSTALL_BINDIR}/..)
  install(FILES sdk/README.txt DESTINATION ${CMAKE_INSTALL_BINDIR}/..)

  if(NOT DEFINED SDK_PLATFORM_NAME)
    message(WARNING "SDK_PLATFORM_NAME is not defined")
  else()
    install(PROGRAMS sdk/${SDK_PLATFORM_NAME}/install_deps.sh
            DESTINATION ${CMAKE_INSTALL_BINDIR}/..)
  endif()

endif()

add_subdirectory(esysrepo)
add_subdirectory(esystest)
