#!/bin/bash

echo "${TXT_S}Boostrap ...${TXT_CLEAR}"
echo "PATH = $PATH"

PATH="$HOME/.local/bin:$PATH"
PATH=~/bin:$PATH

mkdir build_dev
cd build_dev
repo init -u https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/libesys/esysrepo/manifest.git
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_E}Repo init failed.${TXT_CLEAR}"
   exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    N=`sysctl -n hw.logicalcpu`
    sed -i '' -e 's#ssh://git@gitlab.com/libesys#'"https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/libesys"'#g' .repo/manifests/default.xml
else
    N=`nproc --all`
    sed -i 's#ssh://git@gitlab.com/libesys#'"https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/libesys"'#g' .repo/manifests/default.xml
fi

repo sync -j$N
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_W}Repo sync failed.${TXT_CLEAR}"
fi

echo "PATH = $PATH"
echo "${TXT_S}Boostrap Done.${TXT_CLEAR}"
