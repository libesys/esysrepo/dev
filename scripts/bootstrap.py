##
# __legal_b__
##
# Copyright (c) 2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import os
import sys

import esyssdk  # noqa

# This in only needed for SW developement of ESysCIBuild,
# when we want to be able to import ESysCIBuild from the
# super build or simply clone it
esyssdk.find_all_esysbuild(__file__)

import esyscibuild  # noqa


class BootStrap(esyscibuild.SBBootStrap):
    def __init__(self):
        super().__init__("esysrepo_dev")

        self.set_manifest("ssh://git@gitlab.com/libesys/esysrepo/manifest.git")


if __name__ == "__main__":
    bootstrap = BootStrap()
    result = bootstrap.do()
    exit(-result)
