#!/bin/bash

echo "${TXT_S}Build bin SDK ...${TXT_CLEAR}"
echo "PATH = $PATH"

PATH=/snap/bin:$PATH
echo "PATH = $PATH"

mkdir -p build/cmake
cd build/cmake
pwd
if [ -z "$2" ]; then
    cmake ../.. -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_BIN_SDK=On -DSDK_PLATFORM_NAME=$1
elif [ -z "$3" ]; then
    cmake ../.. -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_BIN_SDK=On -DSDK_PLATFORM_NAME=$1 -DUSE_GIT_INSPECTOR=$2
else
    cmake ../.. -DCMAKE_BUILD_TYPE=Release -DSUPERBUILD_BIN_SDK=On -DSDK_PLATFORM_NAME=$1 -DUSE_GIT_INSPECTOR=$2 ${@:3}
fi
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_E}CMake configuration failed.${TXT_CLEAR}"
   exit 1
fi

if [ "$(uname)" == "Darwin" ]; then
    N=`sysctl -n hw.logicalcpu`
else
    N=`nproc --all`
fi

make package -j$N
RESULT=$?
if [ ! ${RESULT} -eq 0 ]; then
   echo "${TXT_E}Binary SDK build failed.${TXT_CLEAR}"
   exit 1
fi

echo "${TXT_S}Build bin SDK done.${TXT_CLEAR}"
