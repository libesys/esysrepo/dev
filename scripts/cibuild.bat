@echo off

call "%~dp0\..\project\msvc\create_python_env.bat"

@echo on
python -m pip install -r "%~dp0\..\project\msvc\requirements.txt"
python -m pip install -r "%~dp0\requirements.txt"

@echo off
call "%~dp0\..\project\msvc\set_env.bat"

set PYTHONPATH=%~dp0src\esyssdk;%ESYSSDK_INST_DIR%\bin

python "%~dp0\cibuild.py" %*
