#!/bin/bash

echo "${TXT_S}Setup Ubuntu 18.04 ...${TXT_CLEAR}"
echo "PATH = $PATH"

cat /etc/lsb-release
uname -a
ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
add-apt-repository ppa:ubuntu-toolchain-r/test
apt update
apt -y install curl
apt -y install wget
apt -y install git
apt -y install update-alternatives
apt -y install python3
update-alternatives --install /usr/bin/python python /usr/bin/python3 20

# Needed because of the usage of GCC 9
apt -y install gcc-9
apt -y install g++-9
apt -y install libstdc++6
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9

PATH="$HOME/.local/bin:$PATH"
git config --global user.name builder
git config --global user.email "builder@libesys.org"
mkdir ~/bin
PATH=~/bin:$PATH
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo

apt -y install build-essential
apt -y install python3-pip

echo "Compile CMake ..."
mkdir temp
cd temp
wget https://github.com/Kitware/CMake/releases/download/v3.22.5/cmake-3.22.5.tar.gz
tar xf cmake-3.22.5.tar.gz
cd cmake-3.22.5
mkdir build
cd build
../bootstrap
make -j`nproc all`
make install
cd ../../..

echo "Compile CMake Done."

apt -y install libpoco-dev
apt -y install software-properties-common
add-apt-repository -y ppa:mhier/libboost-latest
apt -y install libboost1.70-dev
apt -y install doxygen
apt -y install texlive-font-utils
apt -y install lsb-release
apt -y install pkg-config
apt -y install libssh2-1-dev
apt -y install libxml2-dev
apt -y install libxmlsec1-dev
apt -y install graphviz
apt -y install valgrind
apt -y install lcov
wget https://github.com/gohugoio/hugo/releases/download/v0.119.0/hugo_extended_0.119.0_linux-amd64.deb
dpkg -i hugo_extended_0.119.0_linux-amd64.deb
apt -y install clang-format-10
update-alternatives --install /usr/bin/clang-format clang-format /usr/bin/clang-format-10 99
apt -y install clang-tidy-10
update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-10  99
pip3 install conan
pip3 install cmake-format
pip3 install pre-commit
pip3 install black
pip3 install clang-html

echo "PATH = $PATH"
echo "${TXT_S}Setup Ubuntu 18.04 Done.${TXT_CLEAR}"
