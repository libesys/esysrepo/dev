#!/bin/bash

echo "${TXT_S}Setup RockyLinux 9.2 ...${TXT_CLEAR}"

uname -a

dnf -y install epel-release
dnf config-manager --enable crb
dnf -y install dnf-utils
dnf -y install curl
dnf -y install wget
dnf -y install git
if ! [ -x "$(command -v update-alternatives)" ]; then
    dnf -y install update-alternatives
fi
# dnf -y install python-is-python3

PATH="$HOME/.local/bin:$PATH"

git config --global user.name builder
git config --global user.email "builder@libesys.org"

mkdir -p ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+rx ~/bin/repo

dnf -y install gcc gcc-c++ make
dnf -y installlibtool-ltdl-devel
dnf -y install python3-pip
dnf -y install cmake
dnf -y install poco-devel
dnf -y install boost-devel
dnf -y install doxygen
# apt -y install texlive-font-utils
# apt -y install lsb-release
# apt -y install pkg-config
dnf -y install graphviz
dnf -y install valgrind
dnf -y install lcov
# dnf -y install hugo
# dnf -y install libssl-dev
dnf -y install xmlsec1-devel
dnf -y install xmlsec1-openssl-devel
dnf -y install clang-tools-extra    # But this is clang-format 15
# update-alternatives --install /usr/bin/clang-format clang-format /usr/bin/clang-format-14 99
# apt -y install clang-tidy-14
# update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-14  99
pip3 install conan
pip3 install cmake-format
pip3 install pre-commit
pip3 install black
pip3 install clang-html
pip3 install ValgrindCI


echo "${TXT_S}Setup RockyLinux 9.2 Done.${TXT_CLEAR}"
