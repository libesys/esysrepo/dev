#!/bin/bash

echo "${TXT_S}Setup Ubuntu 20.04 ...${TXT_CLEAR}"

LSB_RELEASE_FILE=/etc/lsb-release
if [ -f "$LSB_RELEASE_FILE" ]; then
    cat $LSB_RELEASE_FILE
fi 
uname -a

# Needed by apt update and some docker images don't have it setup 
TMZ_FILE=/etc/timezone
if [ ! -f "$TMZ_FILE" ]; then
    ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
fi
apt update
apt -y install curl
apt -y install wget
apt -y install git
if ! [ -x "$(command -v update-alternatives)" ]; then
    apt -y install update-alternatives
fi
apt -y install python-is-python3

PATH="$HOME/.local/bin:$PATH"
git config --global user.name builder
git config --global user.email "builder@libesys.org"

REPO_FILE=~/bin/repo
if [ ! -f "$REPO_FILE" ]; then
    mkdir -p ~/bin
    PATH=~/bin:$PATH
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo
fi

apt -y install build-essential
apt -y install python3-pip
apt -y install cmake
apt -y install libpoco-dev
apt -y install libboost-all-dev
apt -y install doxygen
apt -y install texlive-font-utils
apt -y install lsb-release
apt -y install pkg-config
apt -y install libssh2-1-dev
apt -y install libxml2-dev
apt -y install libxmlsec1-dev
apt -y install graphvizapt 
apt -y install graphviz
apt -y install valgrind
apt -y install lcov
apt -y install hugo
apt -y libssl-dev
apt -y libxmlsec1-dev
apt -y libxmlsec1-openssl
apt -y install clang-format-10
update-alternatives --install /usr/bin/clang-format clang-format /usr/bin/clang-format-10 99
apt -y install clang-tidy-10
update-alternatives --install /usr/bin/clang-tidy clang-tidy /usr/bin/clang-tidy-10  99
pip3 install conan
pip3 install cmake-format
pip3 install pre-commit
pip3 install black
pip3 install clang-html
pip3 install ValgrindCI

echo "${TXT_S}Setup Ubuntu 20.04 Done.${TXT_CLEAR}"