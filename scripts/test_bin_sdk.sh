#!/bin/bash

echo "${TXT_S}Test bin SDK ...${TXT_CLEAR}"

set -ex

OS=$(uname -s | tr A-Z a-z)

case $OS in
  linux)
    source /etc/os-release
    case $ID in
      debian|mint)
         ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
         apt update
         apt upgrade
         apt -y install bzip2
         ;;

      ubuntu)
        case $VERSION_ID in
          18.04)
          ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
          apt -y update
          apt -y upgrade
          apt -y install bzip2
          ;;

          *)
          ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
          apt update
          apt -y install bzip2
          ;;
        esac
        ;;

      rocky)
        dnf -y update
        dnf -y install bzip2
        ;;

      almalinux)
        dnf -y update
        dnf -y install bzip2
        ;;

      fedora|rhel|centos)
        yum update
        ;;

      *)
        echo -n "unsupported linux distro"
        exit 1
        ;;
    esac
  ;;

  darwin)
    brew update
  ;;

  *)
    echo -n "unsupported OS"
    ;;
esac



ls
tar xf build_dev/build/cmake/esysrepo_dev-*-$1.tar.bz2
ls
cd esysrepo_dev-*-$1
ls -als *
./install_deps.sh
if [ $? -eq 0 ]; then
   echo "The command 'install_deps.sh' succeeded"
else
   echo "The command 'install_deps.sh' failed"
   echo "Test bin SDK failed."
   exit 1
fi

source ./init_sdk
esysrepo version
if [ $? -eq 0 ]; then
   echo "The command 'esysrepo version' succeeded"
else
   echo "The command 'esysrepo version' failed"
   echo "Test bin SDK failed."
   exit 1
fi

echo "${TXT_S}Test bin SDK done.${TXT_CLEAR}"
