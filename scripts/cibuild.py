##
# __legal_b__
##
# Copyright (c) 2022 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import argparse
import logging

s_logger = logging.getLogger(__name__)

from build import MsvcBuild
import bootstrap

import esyssdk  # noqa

# This in only needed for SW developement of ESysCIBuild,
# when we want to be able to import ESysCIBuild from the
# super build or simply clone it
esyssdk.find_all_esysbuild(__file__)

import esyscibuild  # noqa


class CIBuild(esyscibuild.SBCIBuild):
    def __init__(self, logger=None):
        super().__init__()
        self.m_logger = logger
        self.m_bootstrap = bootstrap.BootStrap()
        self.m_build = MsvcBuild()

    def info(self, s):
        if self.m_logger is None:
            return
        self.m_logger.info(s)

    def handle_cmd_line(self):
        self.info("CIBuild.handle_cmd_line start ...")
        parser = argparse.ArgumentParser()
        self.m_build.set_parser(parser)
        group = parser.add_argument_group("CI")
        group.add_argument("--lala", dest="m_lala", action="store_true", help="lala")

        self.m_build.add_param()

        args = parser.parse_args()
        self.m_build.process_param(args)
        self.info("CIBuild.handle_cmd_line end.")


if __name__ == "__main__":
    format = ("%(asctime)s %(levelname)-8s %(message)s",)
    level = (logging.INFO,)
    datefmt = "%Y-%m-%d %H:%M:%S"
    logging.basicConfig(
        filename="cibuild.log",
        format="%(asctime)s %(levelname)-8s %(name)s-%(filename)s:%(lineno)s: %(message)s",
        level=logging.INFO,
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    ci_build = CIBuild(s_logger)
    ci_build.handle_cmd_line()
    result = ci_build.do()
    exit(-result)
