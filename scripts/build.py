##
# __legal_b__
##
# Copyright (c) 2022-2023 Michel Gillet
# Distributed under the MIT License.
# (See accompanying file LICENSE.txt or
# copy at https://opensource.org/licenses/MIT)
##
# __legal_e__
##

import argparse
import os
import sys
import subprocess
import colorama
from colorama import Fore, Back, Style

colorama.init()

import esyssdk

# This in only needed for SW developement of ESysCIBuild,
# when we want to be able to import ESysCIBuild from the
# super build or simply clone it
esyssdk.find_esysbuild(__file__)

import esysbuild  # noqa

from esysbuild import MsvcBuildBase


class MsvcBuild(MsvcBuildBase):
    def __init__(self):
        super().__init__()
        self.set_sln_path("project\\msvc\\esysrepo_dev_all.sln")
        self.set_targets(["esysrepo\\esysrepoexe", "esysrepo\\esysrepo_t"])
        self.set_python_vcxproj(
            [
                ["esyslog", "src\\esyslog\\project\\msvc\\pyesyslog.vcxproj"],
                ["esysrepo", "src\\esysrepo\\project\\msvc\\pyesysrepo.vcxproj"],
            ]
        )
        self.set_iss_template_file("project\\msvc\\esysrepo.j2")
        self.set_iss_file("project\\msvc141\\esysrepo.iss")


if __name__ == "__main__":
    build = MsvcBuild()
    build.set_base_path(__file__, "..")
    result = build.handle_cmd_line()
    result = build.do()
    exit(-result)
