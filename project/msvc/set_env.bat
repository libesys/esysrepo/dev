@echo off

set ESYS_DEV=%~dp0..\..
set ESYSBASE_DEV=%~dp0..\..
set ESYSBUILD_DEV=%~dp0..\..
set ESYSCLI_DEV=%~dp0..\..
set ESYSLOG_DEV=%~dp0..\..
set ESYSREPO_DEV=%~dp0..\..
set ESYSSSH_DEV=%~dp0..\..
set ESYSTEST_DEV=%~dp0..\..
set ESYSTRACE_DEV=%~dp0..\..
set LIBGIT2_DEV=%~dp0..\..
set LIBSSH2_DEV=%~dp0..\..
set MSWORD2MD_DEV=%~dp0..\..

set DBG_LOG_DEV=%~dp0..\..

set ESYSBASE=%~dp0..\..\src\esysbase
set ESYSBUILD=%~dp0..\..\src\esysbuild
set ESYSCIBUILD=%~dp0..\..\src\esyscibuild
set ESYSCLI=%~dp0..\..\src\esyscli
set ESYSFILE=%~dp0..\..\src\esysfile
set ESYSLOG=%~dp0..\..\src\esyslog
set ESYSMSVC=%~dp0..\..\src\esysmsvc
set ESYSREPO=%~dp0..\..\src\esysrepo
set ESYSRES=%~dp0..\..\src\esysres
set ESYSSDK=%~dp0..\..\src\esyssdk
set ESYSSSH=%~dp0..\..\src\esysssh
set ESYSTEST=%~dp0..\..\src\esystest
set ESYSTRACE=%~dp0..\..\src\esystrace
set PYSWIG=%~dp0..\..\extlib\pyswig\src\pyswig
set DBG_LOG=%~dp0..\..\extlib\dbg_log
set LOGMOD=%~dp0..\..\extlib\dbg_log
set TERMCOLOR=%~dp0..\..\extlib\termcolor
set MSWORD2MD=%~dp0..\..\extlib\msword2md
set GIT2=%~dp0..\..\extlib\libgit2
set LIBGIT2=%GIT2%
set ESYS_LIBGIT2=%LIBGIT2%
set KEEP_ESYS_LIBGIT2=1

set LIBSSH2=%~dp0..\..\extlib\libssh2
set ESYS_LIBSSH2=%LIBSSH2%
set KEEP_ESYS_LIBSSH2=1

set ESYSBUILD=%~dp0..\..\src\esysbuild
set ESYSREPO=%~dp0..\..\src\esysrepo

set NLOHMANN_JSON=%~dp0..\..\extlib\nlohmann_json
set SPDLOG=%~dp0..\..\extlib\spdlog
set LIBOPENSSH=%~dp0..\..\extlib\openssh-portable
set LIBOPENSSH_INCLUDES=%LIBOPENSSH%;%LIBOPENSSH%\contrib;%LIBOPENSSH%\contrib\win32;%LIBOPENSSH%\contrib\win32\win32compat\inc;%LIBOPENSSH%\contrib\win32\openssh\LibreSSL\sdk\include
set OPENSSH_INCLUDES=%LIBOPENSSH%;%LIBOPENSSH%\contrib
set DATE=%~dp0..\..\extlib\date

set PYSWIG=%~dp0..\..\extlib\pyswig\src


if defined SWIG_TARGET_PYTHON_VERSION (
    echo SWIG_TARGET_PYTHON_VERSION set to %SWIG_TARGET_PYTHON_VERSION%
) else (
    echo Warning: SWIG_TARGET_PYTHON_VERSION was not set, assign defaults
    set PY_VER=3_10
    set SWIG_TARGET_PYTHON_VERSION=%PY_VER%
    set PYESYS_UNIV_DLL_SUFFIX=_py%PY_VER%
    set PYESYS_UNIV_WARP_PREFIX=py%PY_VER%_
)

set ESYSREPO_SIGN=1
