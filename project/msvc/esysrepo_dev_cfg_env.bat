@echo off

call "%~dp0set_env.bat"
call "%~dp0create_python_env.bat"

@echo on
python -m pip install -r "%~dp0requirements.txt"
call "%~dp0conan_install.bat" %1
