REM @echo off

set OLDDIR=%CD%

conan --version

call "%~dp0..\..\src\esysfile\project\msvc\conan_install.bat" %1

chdir /d %OLDDIR%
