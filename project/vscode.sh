#!/bin/bash

DIR=$(dirname "$(readlink -f "$BASH_SOURCE")")

ESYSCIBUILD_SP_PATH="$DIR/.."
echo "ESYSCIBUILD_SP_PATH = $ESYSCIBUILD_SP_PATH"

#ESYSCIBUILD_DIR="/cygdrive/c/project/libesys/esyssdk_dev/src/esyscibuild"
ESYSCIBUILD_DIR="C:\project\libesys\esyssdk_dev\src\esyscibuild"
echo "ESYSCIBUILD_DIR = $ESYSCIBUILD_DIR"

ESYSREPO_DEV_BIN=`cygpath -w "$ESYSCIBUILD_SP_PATH/bin/vc141_x64_dll"`
echo "ESYSREPO_DEV_BIN = $ESYSREPO_DEV_BIN"

cd $ESYSCIBUILD_SP_PATH
code .
